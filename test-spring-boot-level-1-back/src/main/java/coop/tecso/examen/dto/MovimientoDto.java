package coop.tecso.examen.dto;

import java.io.Serializable;
import java.util.Date;

public class MovimientoDto implements Serializable {

	private static final long serialVersionUID = -1854383574061855612L;

	private Long id;
	private CuentaCorrienteDto cuentaCorriente;
	private Date fecha;
	private TipoMovimientoDto tipoMovimiento;
	private String descripcion;
	private Integer importe;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CuentaCorrienteDto getCuentaCorriente() {
		return cuentaCorriente;
	}

	public void setCuentaCorriente(CuentaCorrienteDto cuentaCorriente) {
		this.cuentaCorriente = cuentaCorriente;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public TipoMovimientoDto getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(TipoMovimientoDto tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getImporte() {
		return importe;
	}

	public void setImporte(Integer importe) {
		this.importe = importe;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
