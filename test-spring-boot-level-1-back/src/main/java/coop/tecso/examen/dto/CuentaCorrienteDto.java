package coop.tecso.examen.dto;

import java.io.Serializable;
import java.util.List;

public class CuentaCorrienteDto implements Serializable {

	private static final long serialVersionUID = -1854383574061855612L;

	private Long id;
	private Integer nroCuenta;
	private MonedaDto moneda;
	private Integer saldo;
	private List<MovimientoDto> movimientos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Integer nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public MonedaDto getMoneda() {
		return moneda;
	}

	public void setMoneda(MonedaDto moneda) {
		this.moneda = moneda;
	}

	public Integer getSaldo() {
		return saldo;
	}

	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}

	public List<MovimientoDto> getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(List<MovimientoDto> movimientos) {
		this.movimientos = movimientos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
