package coop.tecso.examen.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.jandex.Main;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.CuentaCorrienteDto;
import coop.tecso.examen.dto.MonedaDto;
import coop.tecso.examen.dto.MovimientoDto;
import coop.tecso.examen.dto.TipoMovimientoDto;
import coop.tecso.examen.model.CuentaCorriente;
import coop.tecso.examen.model.Movimiento;
import coop.tecso.examen.repository.CuentaCorrienteRepository;
import coop.tecso.examen.repository.MonedaRepository;
import coop.tecso.examen.repository.MovimientoRepository;

@RestController
@RequestMapping("/cuenta")
public class CuentaController {

	private static final Logger logger = LogManager.getLogger(Main.class);

	@Autowired
	private CuentaCorrienteRepository ccRepository;

	@Autowired
	private MonedaRepository monedaRepository;

	@Autowired
	private MovimientoRepository movimientoRepository;

	@CrossOrigin(allowedHeaders = "*")
	@PostMapping("/create/{id}")
	public void Create(@PathVariable(value = "id") String idMoneda, @RequestBody CuentaCorrienteDto cuentaCorriente) {

		CuentaCorriente cuenta = new CuentaCorriente();
		cuenta.setMoneda(monedaRepository.findById(Long.parseLong(idMoneda))
				.orElseThrow(() -> new EntityNotFoundException(idMoneda)));
		cuenta.setNroCuenta(cuentaCorriente.getNroCuenta());
		cuenta.setSaldo(cuentaCorriente.getSaldo());
		ccRepository.save(cuenta);

	}

	@CrossOrigin(allowedHeaders = "*")
	@DeleteMapping("/delete/{id}")
	public String Delete(@PathVariable(value = "id") String idCC) {

		String resultado = "";
		List<Movimiento> movimientosCuenta = movimientoRepository.getMovCta(idCC);

		try {
			if (movimientosCuenta.isEmpty()) {
				ccRepository.deleteById(Long.parseLong(idCC));
				logger.info("Se elimina la cuenta con id " + idCC);
				resultado = "OK";
			} else {
				logger.info("La cuenta id: " + idCC + " tiene " + movimientosCuenta.size()
						+ " movimiento(s), no se puede eliminar ");
				resultado = "KO";
			}
		} catch (Exception e) {
			logger.error(e);
		}

		return resultado;
	}

	@CrossOrigin(allowedHeaders = "*")
	@GetMapping("/getAll")
	public List<CuentaCorrienteDto> getAll() {

		List<CuentaCorrienteDto> result = new ArrayList<>();
		for (CuentaCorriente entity : ccRepository.findAll()) {
			CuentaCorrienteDto dto = new CuentaCorrienteDto();
			dto.setId(entity.getId());
			MonedaDto monDto = new MonedaDto();
			monDto.setId(entity.getMoneda().getId());
			monDto.setDescripcion(entity.getMoneda().getDescripcion());
			monDto.setNombre(entity.getMoneda().getNombre());
			dto.setMoneda(monDto);
			List<MovimientoDto> movimientos = new ArrayList<>();
			for (Movimiento mov : entity.getMovimientos()) {
				MovimientoDto movDto = new MovimientoDto();
				movDto.setId(mov.getId());
				movDto.setDescripcion(mov.getDescripcion());
				movDto.setFecha(mov.getFecha());
				movDto.setImporte(mov.getImporte());
				TipoMovimientoDto tipoMovDto = new TipoMovimientoDto();
				tipoMovDto.setId(mov.getTipoMovimiento().getId());
				tipoMovDto.setDescripcion(mov.getTipoMovimiento().getDescripcion());
				tipoMovDto.setNombre(mov.getTipoMovimiento().getNombre());
				movDto.setTipoMovimiento(tipoMovDto);
				movimientos.add(movDto);
			}
			dto.setMovimientos(movimientos);
			dto.setNroCuenta(entity.getNroCuenta());
			dto.setSaldo(entity.getSaldo());
			result.add(dto);
		}

		return result;
	}

}
