package coop.tecso.examen.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.jandex.Main;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.CuentaCorrienteDto;
import coop.tecso.examen.dto.MonedaDto;
import coop.tecso.examen.dto.MovimientoDto;
import coop.tecso.examen.dto.TipoMovimientoDto;
import coop.tecso.examen.model.CuentaCorriente;
import coop.tecso.examen.model.Movimiento;
import coop.tecso.examen.repository.CuentaCorrienteRepository;
import coop.tecso.examen.repository.MovimientoRepository;
import coop.tecso.examen.repository.TipoMovimientoRepository;

@RestController
@RequestMapping("/movimiento")
public class MovimientoController {

	private static final Logger logger = LogManager.getLogger(Main.class);

	@Autowired
	private MovimientoRepository movRepository;

	@Autowired
	private CuentaCorrienteRepository ccRepository;

	@Autowired
	private TipoMovimientoRepository tmRepository;

	@CrossOrigin(allowedHeaders = "*")
	@PostMapping("/add/{idCc}/{idTipo}")
	public String add(@PathVariable(value = "idCc") String idCuenta, @PathVariable(value = "idTipo") String idMov,
			@RequestBody MovimientoDto movimiento) {

		String resultado = "";
		try {
			Movimiento mov = new Movimiento();
			mov.setCuentaCorriente(ccRepository.findById(Long.parseLong(idCuenta))
					.orElseThrow(() -> new EntityNotFoundException(idCuenta)));
			mov.setTipoMovimiento(
					tmRepository.findById(Long.parseLong(idMov)).orElseThrow(() -> new EntityNotFoundException(idMov)));
			mov.setImporte(movimiento.getImporte());
			mov.setDescripcion(movimiento.getDescripcion());
			mov.setFecha(new Date());

			CuentaCorriente cuenta = ccRepository.getOne(Long.parseLong(idCuenta));

			if (mov.getTipoMovimiento().getId() == 1) {
				cuenta.setSaldo(cuenta.getSaldo() - mov.getImporte());
				if (cuenta.getMoneda().getId() == 1 && cuenta.getSaldo() < -1000) {
					resultado = "ERRP";
					logger.info("La transaccion deja un descubierto mayor a 1000 Pesos");
				} else if (cuenta.getMoneda().getId() == 2 && cuenta.getSaldo() < -300) {
					resultado = "ERRD";
					logger.info("La transaccion deja un descubierto mayor a 300 Dolares");
				} else if (cuenta.getMoneda().getId() == 3 && cuenta.getSaldo() < -150) {
					resultado = "ERRE";
					logger.info("La transaccion deja un descubierto mayor a 150 Euros");
				} else {
					resultado = "OK";
					logger.info("Se registro la transaccion correctamente (Debito)");
					movRepository.save(mov);
					ccRepository.save(cuenta);
				}
			} else if (mov.getTipoMovimiento().getId() == 2) {
				cuenta.setSaldo(cuenta.getSaldo() + mov.getImporte());
				resultado = "OK";
				logger.info("Se registro la transaccion correctamente (Credito)");
				movRepository.save(mov);
				ccRepository.save(cuenta);
			}

		} catch (Exception e) {
			logger.error(e);
		}

		return resultado;

	}

	@GetMapping("/getMovCta/{id}")
	public List<MovimientoDto> getMovCta(@PathVariable(value = "id") String idCuenta) {
		List<MovimientoDto> result = new ArrayList<>();

		for (Movimiento entity : movRepository.getMovCta(idCuenta)) {
			MovimientoDto dto = new MovimientoDto();
			dto.setId(entity.getId());
			CuentaCorrienteDto ccDto = new CuentaCorrienteDto();
			ccDto.setId(entity.getCuentaCorriente().getId());
			MonedaDto monDto = new MonedaDto();
			monDto.setId(entity.getCuentaCorriente().getMoneda().getId());
			monDto.setDescripcion(entity.getCuentaCorriente().getMoneda().getDescripcion());
			monDto.setNombre(entity.getCuentaCorriente().getMoneda().getDescripcion());
			ccDto.setMoneda(monDto);
			ccDto.setNroCuenta(entity.getCuentaCorriente().getNroCuenta());
			ccDto.setSaldo(entity.getCuentaCorriente().getSaldo());
			dto.setCuentaCorriente(ccDto);
			dto.setDescripcion(entity.getDescripcion());
			dto.setFecha(entity.getFecha());
			dto.setImporte(entity.getImporte());
			TipoMovimientoDto tipoMovDto = new TipoMovimientoDto();
			tipoMovDto.setId(entity.getTipoMovimiento().getId());
			tipoMovDto.setDescripcion(entity.getTipoMovimiento().getDescripcion());
			tipoMovDto.setNombre(entity.getTipoMovimiento().getNombre());
			dto.setTipoMovimiento(tipoMovDto);
			result.add(dto);
		}
		return result;
	}

	@GetMapping("/getAll")
	public List<MovimientoDto> getAll() {
		List<MovimientoDto> result = new ArrayList<>();
		for (Movimiento entity : movRepository.findAll()) {
			MovimientoDto dto = new MovimientoDto();
			dto.setId(entity.getId());
			CuentaCorrienteDto ccDto = new CuentaCorrienteDto();
			ccDto.setId(entity.getCuentaCorriente().getId());
			MonedaDto monDto = new MonedaDto();
			monDto.setId(entity.getCuentaCorriente().getMoneda().getId());
			monDto.setDescripcion(entity.getCuentaCorriente().getMoneda().getDescripcion());
			monDto.setNombre(entity.getCuentaCorriente().getMoneda().getDescripcion());
			ccDto.setMoneda(monDto);
			ccDto.setNroCuenta(entity.getCuentaCorriente().getNroCuenta());
			ccDto.setSaldo(entity.getCuentaCorriente().getSaldo());
			dto.setCuentaCorriente(ccDto);
			dto.setDescripcion(entity.getDescripcion());
			dto.setFecha(entity.getFecha());
			dto.setImporte(entity.getImporte());
			TipoMovimientoDto tipoMovDto = new TipoMovimientoDto();
			tipoMovDto.setId(entity.getTipoMovimiento().getId());
			tipoMovDto.setDescripcion(entity.getTipoMovimiento().getDescripcion());
			tipoMovDto.setNombre(entity.getTipoMovimiento().getNombre());
			dto.setTipoMovimiento(tipoMovDto);
			result.add(dto);
		}
		return result;
	}

}
