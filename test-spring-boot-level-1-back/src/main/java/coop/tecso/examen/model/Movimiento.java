package coop.tecso.examen.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Movimiento extends AbstractPersistentObject {

	private static final long serialVersionUID = -8901155893511467206L;

	@ManyToOne
	@JoinColumn(name = "cuenta_corriente_id")
	private CuentaCorriente cuentaCorriente;
	private Date fecha;
	@ManyToOne
	@JoinColumn(name = "tipo_movimiento_id")
	private TipoMovimiento tipoMovimiento;
	private String descripcion;
	private Integer importe;

	public CuentaCorriente getCuentaCorriente() {
		return cuentaCorriente;
	}

	public void setCuentaCorriente(CuentaCorriente cuentaCorriente) {
		this.cuentaCorriente = cuentaCorriente;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public TipoMovimiento getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getImporte() {
		return importe;
	}

	public void setImporte(Integer importe) {
		this.importe = importe;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
