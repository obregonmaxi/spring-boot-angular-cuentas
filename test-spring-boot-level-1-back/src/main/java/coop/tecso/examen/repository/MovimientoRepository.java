package coop.tecso.examen.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import coop.tecso.examen.model.Movimiento;

public interface MovimientoRepository extends JpaRepository<Movimiento, Long> {

	@Query(value = "SELECT * FROM movimiento m WHERE m.cuenta_corriente_id = :cta ORDER BY m.fecha DESC", nativeQuery = true)
	public List<Movimiento> getMovCta(@Param("cta") String cta);

}
