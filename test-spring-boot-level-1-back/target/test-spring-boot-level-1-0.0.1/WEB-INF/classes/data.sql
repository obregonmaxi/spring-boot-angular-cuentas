INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('AR','ARGENTINA', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('BR','BRASIL', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('UY','URUGUAY', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('CH','CHILE', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);


--MONEDAS
INSERT INTO moneda(nombre,descripcion, creation_timestamp, modification_timestamp, version_number)
VALUES ('Peso','$', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO moneda(nombre,descripcion, creation_timestamp, modification_timestamp, version_number)
VALUES ('Dolar','US$', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO moneda(nombre,descripcion, creation_timestamp, modification_timestamp, version_number)
VALUES ('Euro','€', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);


--TIPOS DE MOVIMIENTO
INSERT INTO tipo_movimiento(nombre,descripcion, creation_timestamp, modification_timestamp, version_number)
VALUES ('Debito','Se debita', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO tipo_movimiento(nombre,descripcion, creation_timestamp, modification_timestamp, version_number)
VALUES ('Credito','Se acredita', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

--CUENTAS CORRIENTES
INSERT INTO cuenta_corriente(nro_cuenta, moneda_id, saldo, creation_timestamp, modification_timestamp, version_number)
VALUES (101,1,10000, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO cuenta_corriente(nro_cuenta, moneda_id, saldo, creation_timestamp, modification_timestamp, version_number)
VALUES (102,2,10000, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO cuenta_corriente(nro_cuenta, moneda_id, saldo, creation_timestamp, modification_timestamp, version_number)
VALUES (103,3,10000, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);


--MOVIMIENTOS

INSERT INTO movimiento(cuenta_corriente_id, fecha, tipo_movimiento_id, descripcion, importe, creation_timestamp, modification_timestamp, version_number)
VALUES (1, CURRENT_TIMESTAMP(), 2, 'Ingreso de efectivo por caja', 1000, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movimiento(cuenta_corriente_id, fecha, tipo_movimiento_id, descripcion, importe, creation_timestamp, modification_timestamp, version_number)
VALUES (2, CURRENT_TIMESTAMP(), 2, 'Pago de salario', 1000, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movimiento(cuenta_corriente_id, fecha, tipo_movimiento_id, descripcion, importe, creation_timestamp, modification_timestamp, version_number)
VALUES (3, CURRENT_TIMESTAMP(), 2, 'Transferencia bancaria', 1000, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movimiento(cuenta_corriente_id, fecha, tipo_movimiento_id, descripcion, importe, creation_timestamp, modification_timestamp, version_number)
VALUES (1, CURRENT_TIMESTAMP(), 1, 'Compra con tarjeta', 100, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);