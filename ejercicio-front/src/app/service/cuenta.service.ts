import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const baseUrl = 'http://localhost:8080/api/cuenta';


@Injectable({
    providedIn: 'root'
})
export class CuentaService {

    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get(baseUrl + '/getAll');
    }

    get(id) {
        return this.http.get(`${baseUrl}/${id}`);
    }

    create(data, idMoneda) {
        return this.http.post(`${baseUrl}/create/${idMoneda}`, data);
    }

    update(id, data) {
        return this.http.put(`${baseUrl}/${id}`, data);
    }

    delete(id) {
        const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        return this.http.delete(`${baseUrl}/delete/${id}`, { headers, responseType: "text" });
    }

    deleteAll() {
        return this.http.delete(baseUrl);
    }

    findByTitle(title) {
        return this.http.get(`${baseUrl}?title=${title}`);
    }
}
