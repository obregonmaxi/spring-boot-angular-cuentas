import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const baseUrl = 'http://localhost:8080/api/movimiento';


@Injectable({
    providedIn: 'root'
})
export class MovimientoService {

    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get(baseUrl);
    }

    get(id) {
        return this.http.get(`${baseUrl}/${id}`);
    }

    create(data, idCuenta, idTipoMovimiento) {
        const headers = new HttpHeaders();
        return this.http.post(`${baseUrl}/add/${idCuenta}/${idTipoMovimiento}`, data, { headers, responseType: "text" });
    }

    update(id, data) {
        return this.http.put(`${baseUrl}/${id}`, data);
    }

    delete(id) {
        return this.http.delete(`${baseUrl}/${id}`);
    }

    deleteAll() {
        return this.http.delete(baseUrl);
    }

    findByTitle(title) {
        return this.http.get(`${baseUrl}?title=${title}`);
    }
}
