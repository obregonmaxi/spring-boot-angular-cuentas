import { Component, OnInit } from '@angular/core';
import { CuentaService } from '../../service/cuenta.service';
import { CuentaCorriente } from '../../model/CuentaCorriente';
import { Moneda } from 'src/app/model/Moneda';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-cuenta',
  templateUrl: './add-cuenta.component.html',
  styleUrls: ['./add-cuenta.component.scss']
})
export class AddCuentaComponent implements OnInit {

  cuenta: CuentaCorriente;
  submitted = false;
  monedas: Moneda[];
  currentIndex = -1;
  cuentas: CuentaCorriente[];

  constructor(private ccService: CuentaService, private snackBar: MatSnackBar) {
    this.inicializarCuenta();
    //Se omite servicio de recuperacion de monedas
    this.monedas = new Array<Moneda>();
    let peso = new Moneda();
    let dolar = new Moneda();
    let euro = new Moneda();
    peso.id = 1;
    peso.nombre = "Peso";
    peso.descripcion = "$";
    dolar.id = 2;
    dolar.nombre = "Dolar";
    dolar.descripcion = "US$"
    euro.id = 3;
    euro.nombre = "Euro";
    euro.descripcion = "€"
    this.monedas.push(peso);
    this.monedas.push(dolar);
    this.monedas.push(euro);
  }

  cuentaForm: FormGroup;

  inicializarForm() {
    this.cuentaForm = new FormGroup({
      'moneda': new FormControl(this.cuenta.moneda, [
        Validators.required
      ]),
      'nroCuenta': new FormControl(this.cuenta.nroCuenta, [
        Validators.required
      ]),
      'saldo': new FormControl(this.cuenta.saldo, [
        Validators.required
      ])
    });
  }

  get moneda() { return this.cuentaForm.get('moneda'); }
  get nroCuenta() { return this.cuentaForm.get('nroCuenta'); }
  get saldo() { return this.cuentaForm.get('saldo'); }

  inicializarCuenta() {
    this.cuenta = new CuentaCorriente();
    this.cuenta.saldo = 0;
    this.cuenta.nroCuenta = 0;
    this.inicializarForm();
  }
  ngOnInit() {
    this.retrieveCuentas();
  }

  retrieveCuentas() {
    this.ccService.getAll()
      .subscribe(
        (data: CuentaCorriente[]) => {
          this.cuentas = data;
          console.log(data);
        },
        error => {
          console.log(error);
          this.snackBar.open("No se pudo conectar con el servicio", "OK", { duration: 1000 });
        });
  }

  saveCuenta() {
    this.cuenta.nroCuenta = this.nroCuenta.value;
    this.cuenta.saldo = this.saldo.value;
    let existe = false;
    this.cuentas.forEach(c => {
      if (this.cuenta.nroCuenta == c.nroCuenta) {
        existe = true;
      }
    });
    if (existe) {
      this.snackBar.open("El numero de cuenta ya existe en la base de datos", "OK", { duration: 1000 })
    } else {
      this.ccService.create(this.cuenta, this.moneda.value)
        .subscribe(
          response => {
            this.snackBar.open("Se creo la cuenta correctamente", "OK", { duration: 1000 })
            console.log(response);
            this.submitted = true;
          },
          error => {
            console.log(error);
          });
    }
  }

  newCuenta() {
    this.submitted = false;
    this.inicializarCuenta();
  }

  setActiveMoneda(moneda: Moneda) {
    this.cuenta.moneda = moneda;
  }

}
