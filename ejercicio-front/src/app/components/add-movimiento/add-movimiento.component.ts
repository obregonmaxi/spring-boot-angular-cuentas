import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MovimientoService } from 'src/app/service/movimiento.service';
import { Movimiento } from '../../model/Movimiento';
import { CuentaCorriente } from 'src/app/model/CuentaCorriente';
import { TipoMovimiento } from '../../model/TipoMovimiento';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-movimiento',
  templateUrl: './add-movimiento.component.html',
  styleUrls: ['./add-movimiento.component.scss']
})
export class AddMovimientoComponent implements OnInit {

  @Input() currentCuenta: CuentaCorriente;
  @Output() updateCuenta = new EventEmitter<any>();

  movimiento: Movimiento;

  tiposMovimiento: TipoMovimiento[];

  constructor(private movimientoService: MovimientoService, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.movimiento = new Movimiento();
    this.movimiento.fecha = new Date();
    this.movimiento.cuentaCorriente = this.currentCuenta;
    this.movimiento.descripcion = "";
    this.movimiento.importe = 0;
    //Se omite el servicio que recupera los tipos de movimiento
    this.tiposMovimiento = new Array<TipoMovimiento>();
    let debito = new TipoMovimiento;
    debito.id = 1;
    debito.nombre = "Debito";
    debito.descripcion = "Se debita"
    this.tiposMovimiento.push(debito);
    let credito = new TipoMovimiento;
    credito.id = 2;
    credito.nombre = "Credito";
    credito.descripcion = "Se acredita"
    this.tiposMovimiento.push(credito);
    this.inicializarForm()
  }

  movimientoForm: FormGroup;

  inicializarForm() {
    this.movimientoForm = new FormGroup({
      'tipoMovimiento': new FormControl(this.movimiento.tipoMovimiento, [Validators.required]),
      'importe': new FormControl(this.movimiento.importe, [Validators.required]),
      'descripcion': new FormControl(this.movimiento.descripcion, [Validators.required]),
    });
  }

  get tipoMovimiento() { return this.movimientoForm.get('tipoMovimiento'); }
  get importe() { return this.movimientoForm.get('importe'); }
  get descripcion() { return this.movimientoForm.get('descripcion'); }


  onAgregarMovimiento() {
    this.movimiento.descripcion = this.descripcion.value;
    this.movimiento.importe = this.importe.value;
    this.movimientoService.create(this.movimiento, this.currentCuenta.id, this.tipoMovimiento.value).subscribe(x => {
      if (x == "OK") {
        this.snackBar.open("Se registro el movimietno correctamente", "OK", { duration: 1000 });
        this.updateCuenta.emit();
      } else if (x == "ERRP") {
        this.snackBar.open("El movimietno deja un descubierto mayor a 1000 Pesos", "OK", { duration: 1000 });
      } else if (x == "ERRD") {
        this.snackBar.open("El movimietno deja un descubierto mayor a 300 Dolares", "OK", { duration: 1000 });
      } else if (x == "ERRE") {
        this.snackBar.open("El movimietno deja un descubierto mayor a 150 Euros", "OK", { duration: 1000 });
      }
    },
      error => {
        console.log(error);
        this.snackBar.open("No se pudo conectar con el servicio", "OK", { duration: 1000 });
      });
  }

}