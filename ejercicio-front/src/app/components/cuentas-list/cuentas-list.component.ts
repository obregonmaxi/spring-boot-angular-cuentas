import { Component, OnInit } from '@angular/core';
import { CuentaService } from 'src/app/service/cuenta.service';
import { CuentaCorriente } from 'src/app/model/CuentaCorriente';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-cuentas-list',
  templateUrl: './cuentas-list.component.html',
  styleUrls: ['./cuentas-list.component.scss']
})
export class CuentasListComponent implements OnInit {

  cuentas: any;
  currentCuenta = null;
  currentIndex = -1;
  add = false;

  constructor(private cuentaService: CuentaService, private snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
    this.retrieveCuentas();
  }

  updateCuenta() {
    this.add = false;
    this.refreshList()
  }

  onAgregarMovimiento() {
    this.add = true;
  }

  retrieveCuentas() {
    this.cuentaService.getAll()
      .subscribe(
        (data: CuentaCorriente[]) => {
          this.cuentas = data;
          console.log(data);
        },
        error => {
          console.log(error);
          this.snackBar.open("No se pudo conectar con el servicio", "OK", { duration: 1000 });
        });
  }

  refreshList() {
    this.retrieveCuentas();
    this.currentCuenta = null;
    this.currentIndex = -1;
  }

  setActiveCuenta(cuenta, index) {
    this.currentCuenta = cuenta;
    this.currentIndex = index;
    this.add = false;
  }

  borrarCuenta(cuenta, index) {
    this.cuentaService.delete(cuenta.id).subscribe(x => {
      if (x == "OK") {
        this.snackBar.open("Se borro la cuenta con exito", "OK", { duration: 1000 });
        this.refreshList();
      } else if (x == "KO") {
        this.snackBar.open("La cuenta tiene movimientos, no se puede eliminar", "OK", { duration: 1000 });
      }
    },
      error => {
        console.log(error);
        this.snackBar.open("No se pudo conectar con el servicio", "OK", { duration: 1000 });
      });
  }
}
