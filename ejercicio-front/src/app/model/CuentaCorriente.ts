import { Moneda } from './Moneda';
import { Movimiento } from './Movimiento';

export class CuentaCorriente {
    id: number;
    nroCuenta: number;
    moneda: Moneda;
    saldo: number;
    movimientos: Movimiento[];
}