import { CuentaCorriente } from './CuentaCorriente';
import { TipoMovimiento } from './TipoMovimiento';

export class Movimiento {
    id: number;
    cuentaCorriente: CuentaCorriente
    fecha: Date;
    tipoMovimiento: TipoMovimiento;
    descripcion: string;
    importe: number;
}