import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CuentasListComponent } from './components/cuentas-list/cuentas-list.component';
import { AddCuentaComponent } from './components/add-cuenta/add-cuenta.component';



const routes: Routes = [
  { path: '', redirectTo: 'cuentas', pathMatch: 'full' },
  { path: 'cuentas', component: CuentasListComponent },
  { path: 'addCuenta', component: AddCuentaComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
